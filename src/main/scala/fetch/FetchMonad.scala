package fetch

import cats.Monad
import cats.effect.IO
import cats.effect.concurrent.Ref
import cats.implicits._

object FetchMonad {
  /**
   * 类似一个 Future, 只会 resolve 这个 request 一次
   * ref 里面记录了是否已经 resolve
   * request 指的是 blocked by this request
   */
  case class BlockedRequest[A](request: Request[A],
                               ref: Ref[IO, FetchStatus[A]])

  sealed trait FetchStatus[+A]

  case object NotFetched extends FetchStatus[Nothing]

  case class FetchSuccess[A](a: A) extends FetchStatus[A]

  sealed trait Result[A]

  case class Done[A](a: A) extends Result[A]

  /**
   * 一个 Result 被 blocked 了, 当 br 都 resolve 的时候, 继续执行 cont
   * @param br   blocked by this requests
   * @param cont continuation, when br are all resolved, continue
   */
  case class Blocked[A, R](br: List[BlockedRequest[R]], cont: Fetch[A]) extends Result[A]

  case class Fetch[A](r: IO[Result[A]])

  implicit val fetchMonad: Monad[Fetch] = new Monad[Fetch] {
    override def pure[A](x: A): Fetch[A] = Fetch(Done(x).pure[IO])

    override def map[A, B](fa: Fetch[A])(f: A => B): Fetch[B] = Fetch {
      val io: IO[Result[B]] = for {
        result <- fa.r
        newResult <- result match {
          case Done(a) => Done(f(a)).pure[IO]
          case Blocked(br, cont) => Blocked(br, map(cont)(f)).pure[IO]
        }
      } yield newResult

      io
    }

    override def map2[A, B, Z](fa: Fetch[A], fb: Fetch[B])(f: (A, B) => Z): Fetch[Z] =
      Fetch {
        val io: IO[Result[Z]] = for {
          ab <- (fa.r, fb.r).tupled
          result <- ab match {
            case (Done(a), Done(b)) => Done(f(a, b)).pure[IO]
            case (Done(_), Blocked(br, cont)) =>
              Blocked(br, map2(fa, cont)(f)).pure[IO]
            case (Blocked(br, cont), Done(_)) =>
              Blocked(br, map2(cont, fb)(f)).pure[IO]
            case (Blocked(br1, cont1), Blocked(br2, cont2)) =>
              Blocked(br1 ++ br2, map2(cont1, cont2)(f)).pure[IO]
          }
        } yield result

        io
      }

    override def flatMap[A, B](fa: Fetch[A])(f: A => Fetch[B]): Fetch[B] =
      Fetch {
        val io: IO[Result[B]] = fa.r.flatMap {
          case Done(a) => f(a).r
          case Blocked(br, cont) => Blocked(br, flatMap(cont)(f)).pure[IO]
        }

        io
      }

    override def tailRecM[A, B](a: A)(f: A => Fetch[Either[A, B]]): Fetch[B] =
      ???
  }

  def dataFetch[A](request: Request[A]): Fetch[A] = {
    val io: IO[Blocked[A, A]] = for {
      box <- Ref.of[IO, FetchStatus[A]](NotFetched)
      br = BlockedRequest(request, box)
      cont = Fetch(for {
        FetchSuccess(a) <- box.get
      } yield Done(a))
    } yield Blocked(List(br), cont)

    Fetch(io)
  }

  /**
   * 输入 query plan 和 interpreter, 返回 query result
   * @param fetchFunc Request 怎么 resolve 的函数, 类似 interpreter
   * @param toFetch 需要 resolve 的业务逻辑, 类似 query plan
   */
  def runFetch[A](fetchFunc: List[BlockedRequest[Any]] => IO[Unit],
                  toFetch: Fetch[A]): IO[A] =
    for {
      r <- toFetch.r
      a <- r match {
        case Done(a) => a.pure[IO]
        case Blocked(br, cont) =>
          for {
            _ <- fetchFunc(br)
            contA <- runFetch(fetchFunc, cont)
          } yield contA
      }
    } yield a

  type PostId = String

  case class PostInfo()

  case class PostContent()

  /**
   * 一个抽象的网络/数据库请求
   *
   * @tparam A Return type of this Request
   */
  trait Request[+A]
}
