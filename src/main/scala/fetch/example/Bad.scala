package fetch.example

import cats.Monad
import cats.effect.IO
import cats.implicits._
import fetch.example.Models._

object Bad {
  /**
   * 一个 class 里面的所有 student info
   */
  def studentInfosByClass[F[_]: Monad](classId: ClassId): F[List[StudentInfo]] =
    for {
      students <- Database.studentByClass[F](classId)
      studentInfos <- students.traverse { student =>
        Database.studentInfoById[F](student.studentId)
      }
    } yield studentInfos

  def studentInfosByClass_[F[_]: Monad](
    classId: ClassId
  ): F[List[StudentInfo]] =
    for {
      students <- Database.studentByClass[F](classId)
      studentIds = students.map(_.studentId)
      studentInfos <- Database.studentInfosById[F](studentIds)
    } yield studentInfos

  def studentInfosByGrade[F[_]: Monad](gradeId: GradeId): F[List[StudentInfo]] =
    for {
      classes <- Database.classByGrade[F](gradeId)
      studentInfos <- classes.traverse { cls =>
        studentInfosByClass[F](cls.classId)
      }
    } yield studentInfos.flatten

  def studentInfosByGrade_[F[_]: Monad](
    gradeId: GradeId
  ): F[List[StudentInfo]] =
    for {
      classes <- Database.classByGrade[F](gradeId)
      classIds = classes.map(_.classId)
      students <- Database.studentsByClass[F](classIds)
      // 下面这两行代码和studentInfosByClass_里面是完全一样的，但是没办法复用
      studentIds = students.map(_.studentId)
      studentInfos <- Database.studentInfosById[F](studentIds)
    } yield studentInfos

  def main(args: Array[String]): Unit = {
    val program = studentInfosByGrade[IO]("grade1")
    val res = program.unsafeRunSync()
    println(1)
  }
}
