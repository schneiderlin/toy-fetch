package fetch.example

// 有很多个 grade
// 一个 grade 里面有很多的 class
// 一个 class 里面有很多的 student
// 每个 student 有一个对应的 student info
object Models {
  type StudentId = String
  type ClassId = String
  type GradeId = String

  case class Student(studentId: StudentId, classId: ClassId)
  case class StudentInfo(studentId: StudentId, name: String)
  case class Class(classId: ClassId, gradeId: GradeId)
}
