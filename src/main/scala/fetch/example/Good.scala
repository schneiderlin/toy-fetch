package fetch.example

import cats.effect.IO
import cats.implicits._
import fetch.FetchMonad._
import fetch.example.Database._
import fetch.example.Models._

object Good {
  // ------------ 一些boilerplate
  // scala中有一个Fetch library，不但可以batch同源的请求，还有cache请求结果的功能。
  // https://47deg.github.io/fetch/docs.html
  // 使用Fetch library可以去掉这些boilerplate
  // 这里实现的是一个最小功能的Fetch Monad，do not use in production
  case class StudentInfoRequest(studentId: StudentId) extends Request[StudentInfo]

  case class StudentByClassRequest(classId: ClassId) extends Request[List[Student]]

  case class ClassByGradeRequest(gradeId: GradeId) extends Request[List[Class]]

  /**
   * how to resolve basic unit
   */
  def myFetchFunc(requests: List[BlockedRequest[Any]]): IO[Unit] = {
    // 把 requests 按照类型聚合, 同类型的 requests 放在一起
    val partitionedRequest: Map[String, List[BlockedRequest[Any]]] = requests
      .map {
        case r@BlockedRequest(StudentInfoRequest(_), _) => "studentInfo" -> r
        case r@BlockedRequest(StudentByClassRequest(_), _) => "studentByClass" -> r
        case r@BlockedRequest(ClassByGradeRequest(_), _) => "classByGrade" -> r
      }
      .groupBy(_._1)
      .mapValues(_.map(_._2))

    // 同时把这些聚类好的请求发到数据库去 resolve
    for {
      _ <- partitionedRequest
        .map {
          case (key, rs) =>
            key match {
              case "studentInfo" =>
                resolveStudentInfo(rs.asInstanceOf[List[BlockedRequest[StudentInfo]]])
              case "studentByClass" =>
                resolveStudentByClass(rs.asInstanceOf[List[BlockedRequest[List[Student]]]])
              case "classByGrade" =>
                resolveClassByGrade(rs.asInstanceOf[List[BlockedRequest[List[Class]]]])
            }
        }
        .toList
        .sequence[IO, Unit]
    } yield ()
  }

  def resolveStudentInfo(blocks: List[BlockedRequest[StudentInfo]]): IO[Unit] = {
    val requests: List[StudentInfoRequest] = blocks.map(_.request).asInstanceOf[List[StudentInfoRequest]]
    // 获取需要批量查的 ids
    val studentIds: List[StudentId] = requests.map(_.studentId)
    for {
      // 去数据库批量查, 然后拿到个 id -> value 的 map
      resultMap <- studentInfosById[IO](studentIds).map {
        _.map(info => info.studentId -> info).toMap
      }
      // 把查到的值 set 到 blocks 里面, resolve 每一个 blocks
      _ <- blocks.traverse { block =>
        val request: StudentInfoRequest =
          block.request.asInstanceOf[StudentInfoRequest]
        block.ref.set(FetchSuccess(resultMap(request.studentId)))
      }
    } yield ()
  }

  /**
   * 类似上面那个函数, 上面那个很多注释
   */
  def resolveStudentByClass(blocks: List[BlockedRequest[List[Student]]]): IO[Unit] = {
    val requests: List[StudentByClassRequest] = blocks.map(_.request).asInstanceOf[List[StudentByClassRequest]]
    val classIds: List[ClassId] = requests.map(_.classId)
    for {
      resultMap <- studentsByClass[IO](classIds).map {
        _.groupBy(student => student.classId)
      }
      _ <- blocks.traverse { block =>
        val request: StudentByClassRequest =
          block.request.asInstanceOf[StudentByClassRequest]
        block.ref.set(FetchSuccess(resultMap(request.classId)))
      }
    } yield ()
  }

  /**
   * 类似上面那个函数, 上面那个很多注释
   */
  def resolveClassByGrade(blocks: List[BlockedRequest[List[Class]]]): IO[Unit] = {
    val requests: List[ClassByGradeRequest] = blocks.map(_.request).asInstanceOf[List[ClassByGradeRequest]]
    val gradeIds: List[GradeId] = requests.map(_.gradeId)
    for {
      resultMap <- classesByGrade[IO](gradeIds).map {
        _.groupBy(cls => cls.gradeId)
      }
      _ <- blocks.traverse { block =>
        val request: ClassByGradeRequest =
          block.request.asInstanceOf[ClassByGradeRequest]
        block.ref.set(FetchSuccess(resultMap(request.gradeId)))
      }
    } yield ()
  }

  // ------------ end of boilerplate

  def studentInfoByClass(classId: ClassId): Fetch[List[StudentInfo]] =
    for {
      students <- dataFetch(StudentByClassRequest(classId))
      studentInfos <- students.traverse { student =>
        dataFetch(StudentInfoRequest(student.studentId))
      }
    } yield studentInfos

  def studentByGrade(gradeId: GradeId): Fetch[List[StudentInfo]] =
    for {
      classes <- dataFetch(ClassByGradeRequest(gradeId))
      // 复用代码并且不需要关心性能问题
      studentInfos <- classes.traverse { cls =>
        studentInfoByClass(cls.classId)
      }
    } yield studentInfos.flatten

  def main(args: Array[String]): Unit = {
    val program = runFetch(myFetchFunc, studentByGrade("grade1"))
    println(studentByGrade("grade1"))
    val res = program.unsafeRunSync()
    println(1)
  }

  // 开始执行 top level 的 fetch
  // fetch 就是一个 IO 任务
  // fetch 展开, 发现这个 top level 的 fetch 有很多的 dependencies
  // 用 interpreter 把这些 dependencies 都完成 (递归)
  // 继续展开 下一个 level 的 fetch (可能会继续递归)
  // 最终会去到叶节点, 不需要继续递归, 用 interpreter 获取到数据之后, 就返回 Done(value), 然后返回上层了

  // fetch 是用 monadic 的方式组合起来的
  // 组合一个 top level 的 fetch, 用的是很多个小的 fetch
  // 这些小的 fetch 在构建大 fetch 的时候, 如果是 applicative 的, 就会把 id batch 起来.
  // 如果是 monadic 的, 就会 query plan 里面加深一层.
  // interpreter 在 resolve query plan 的时候, 有多少层就会执行 myFetchFunc 多少次
}
