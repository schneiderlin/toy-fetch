package fetch.example

import cats.Id
import cats.effect.IO
import fetch.FetchMonad.runFetch
import fetch.example.Good.{myFetchFunc, studentByGrade}

object Demo {
  def main(args: Array[String]): Unit = {
    goodExample()
  }

  private def goodExample(): Unit = {
    val program: IO[List[Models.StudentInfo]] = runFetch(myFetchFunc, Good.studentByGrade("grade1"))
    val res = program.unsafeRunSync()
    println(res)
  }

  private def badExample(): Unit = {
    val value = Bad.studentInfosByClass[Id]("Class1")
    println(value)
  }

  private def optimizeBad(): Unit = {
    val value = Bad.studentInfosByClass_[Id]("Class1")
    println(value)
  }

  private def databaseDemo(): Unit = {
    val value = Database.studentsByClass[Id](List("Class1", "Class2"))
    println(value)
  }
}
