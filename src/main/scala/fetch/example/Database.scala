package fetch.example

import cats.implicits._
import cats.{Applicative, Monad}
import fetch.example.Models._

object Database {
  def studentByClass[F[_]: Applicative](classId: ClassId): F[List[Student]] =
    for {
      _ <- println("query student by class").pure[F]
    } yield List(Student("student1", classId), Student("student2", classId))

  def studentsByClass[F[_]: Applicative](classIds: List[ClassId]): F[List[Student]] =
    for {
      _ <- println("batch query student by class").pure[F]
    } yield
      classIds.flatMap(
        classId =>
          List(Student("student1", classId), Student("student2", classId))
      )

  def studentInfoById[F[_]: Applicative](studentId: StudentId): F[StudentInfo] =
    for {
      _ <- println("query student info").pure[F]
    } yield StudentInfo(studentId, "name")

  def studentInfosById[F[_]: Applicative](studentIds: List[StudentId]): F[List[StudentInfo]] =
    for {
      _ <- println("batch query student info").pure[F]
    } yield studentIds.map(studentId => StudentInfo(studentId, "name"))

  def classByGrade[F[_]: Applicative](gradeId: GradeId): F[List[Class]] =
    for {
      _ <- println("query class by grade").pure[F]
    } yield List(Class("class1", gradeId), Class("class2", gradeId))

  def classesByGrade[F[_]: Applicative](gradeIds: List[GradeId]): F[List[Class]] =
    for {
      _ <- println("query class by grade").pure[F]
    } yield
      gradeIds.flatMap(
        gradeId => List(Class("class1", gradeId), Class("class2", gradeId))
      )
}
