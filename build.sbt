import sbt.Keys._

scalaVersion in ThisBuild := Settings.versions.scala
scalacOptions in ThisBuild ++= Settings.scalacOptions

lazy val root = (project in file("."))
  .settings(
    addCompilerPlugin("com.olegpy"      %% "better-monadic-for" % "0.3.0"),
    libraryDependencies ++= Settings.serverDependencies.value,
    excludeDependencies ++= Settings.excludeDependencies.value,
  )