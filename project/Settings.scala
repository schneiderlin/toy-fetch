import sbt._

object Settings {
  val name = "toy-fetch"
  val version = "1.0"
  val scalacOptions = Seq(
    "-feature",
    "-deprecation",
    "-unchecked",
    "-language:postfixOps",
    "-language:higherKinds",
    "-Ypartial-unification"
  )

  object versions {
    val scala = "2.12.4"
  }

  val serverDependencies = Def.setting(
    Seq(
      // cats
      "org.typelevel" %% "cats-core" % "1.6.0",
      "org.typelevel" %% "cats-effect" % "1.2.0"
    )
  )

  val excludeDependencies = Def.setting(Seq("org.typelevel" % "scala-library"))
}
